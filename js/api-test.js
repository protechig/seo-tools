var app = new Vue({
  el: "#page-test",
  data: {
    sites: null,
    siteName: '',
    formFields: [{groupName: '', urls: [{url: ''}],}],
    modalData: {}
  },
  mounted: function() {
    var elem = document.getElementById('create-test');
    var instance = M.Modal.init(elem, {});
    fetch("/wp-json/seoapi/v1/page-test", {
      method: "GET",
      credentials: "same-origin",
      headers: {
        "X-WP-Nonce": wpApiSettings.nonce
      }
    })
      .then(response => response.json())
      .then(json => (this.sites = json));
  },
  methods: {
      runTest: function(key) {
        apiTest(key);
          M.toast({html: 'Running test, please check back in a few minutes'});
      },
    submitTest: function(key) {
      console
      var data = [ {title: this.siteName}, this.formFields ];
      fetch('/wp-json/seoapi/v1/page-test/create', {
        body: JSON.stringify(data),
        method: "POST",
        credentials: 'same-origin',
        headers: {
          "X-WP-Nonce": wpApiSettings.nonce,
          "Content-type": "application/json"
        }
      })
    },
    addField: function() {
      this.formFields.push({groupName: '', urls: [{url: ''}],});
    },
    addUrl: function(index) {
      this.formFields[index].urls.push({url: ''});
    },
    showModal: function(key) {
      if (this.modalData[key]) {
        var elem = document.querySelector("#modal-" + key);
        var instance = M.Modal.getInstance(elem);
        instance.open();
        console.log(JSON.stringify(this.modalData[key]));
      } else {
        fetch("/wp-json/seoapi/v1/page-test/" + key, {
          method: "GET",
          credentials: "same-origin",
          headers: {
            "X-WP-Nonce": wpApiSettings.nonce
          }
        })
          .then(res => {
            return res.json();
          })
          .then(json => Vue.set(this.modalData, key, json))
          .then(() => {
            var elem = document.querySelector("#modal-" + key);
            var instance = M.Modal.init(elem, {}, false, "modal-" + key);
            instance.open();
          });
      }
    }
  }
});

function apiTest(key) {
  var postInfo = {
    postID: key,
    urls: [
      {
        name: "Product Pages",
        urls: [
          "http://localhost:8081",
          "http://xyzyxyzy.xyz:5555"
        ]
      }
    ]
  } 
  console.log(JSON.stringify(postInfo));
    
  fetch('https://api.zachrussell.net/page-test', {
    body: JSON.stringify(postInfo),
    mode: 'no-cors',
    method: 'POST',
    headers: {
      "Content-type": "application/json"
    },
  });
}