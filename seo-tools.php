<?php
/**
 Plugin Name: SEO Tools
 */

 add_action('wp_enqueue_scripts', 'seoapi_register_scripts');
 function seoapi_register_scripts() {
    wp_register_script( 'vue', 'https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.16/vue.min.js', array(), '2.5.16', true );
    wp_register_script( 'seo-tools-api', plugins_url( '/js/api-test.js', __FILE__ ), array('vue', 'wp-api'), '', true);
 }
 add_action('rest_api_init', function () {
    register_rest_route('seoapi/v1/', '/page-test', array(
        'methods' => 'GET',
        'callback' => 'seoapi_list_sites',
    ));
    register_rest_route('seoapi/v1/', '/page-test/(?P<id>[\d]+)', array(
        'methods' => 'GET',
        'callback' => 'seoapi_get_results',
    ));
    register_rest_route('seoapi/v1/', '/page-test/(?P<id>[\d]+)', array(
        'methods' => 'POST',
        'callback' => 'seoapi_add_result',
    ));
    register_rest_route('seoapi/v1/' , 'page-test/create', array(
        'methods' => 'POST',
        'callback' => 'seoapi_create_test'
    ));
});

function seoapi_create_test($request) {
    $body = $request->get_json_params();
    $title = $body[0]["title"];
    $config = json_encode($body[1]);
    wp_insert_post(array(
        'post_title'    => $title,
        'post_type'     => 'checks',
        'meta_input'    => array(
            'config'    => $config,
        ),
        'post_status' => 'publish',
    ));

    return($config);
}
function seoapi_add_result($request)
{
    $body = $request->get_json_params();
    $id = (string) $request['id'];
    $update =  add_post_meta($id, 'response_data', json_encode($body));
    return $update;
}
function seoapi_get_results($request)
{
    $id = (string) $request['id'];
    $post_data = get_post($id);
    $post_meta = get_post_meta($id, 'response_data');
    $data = array(
        'post_data' => $post_data,
        'response_data' => $post_meta,
    );

    return new WP_REST_Response($data, 200);
}

function seoapi_list_sites($data)
{
    if (!is_user_logged_in()) {
        return new \WP_Error(
            'rest_forbidden',
            esc_html__('Sorry, you are not allowed to do that.', 'wpse'),
            ['status' => 403]
        );
    }
    $args = array(
        'post_type' => array('checks'),
        'author' => get_current_user_id(),
    );

    // The Query
    $query = new WP_Query($args);

    return ($query->posts);
}

function seo_tools_shortcode() {
    if( is_user_logged_in()):
    $html = <<<EOT
    <div id="page-test">
    <button data-target="create-test" class="btn modal-trigger">Create SEO
Test<i class="material-icons left">add</i></button>
    <table class="striped">
        <thead>
            <tr>
                <td>Run Test</td>
                <td>Site Name</td>
                <td>Action</td>
            </tr>
        </thead>
        <tbody>
        <tr v-for="site in sites" :key="site.ID">
            <td><button @click="runTest(site.ID)" class="btn-floating btn-large waves-effect"><i class="material-icons">play_arrow</i></button></td>
            <td>{{ site.post_title }}</td>
            <td>
                <button v-on:click="showModal(site.ID)" class="btn modal-trigger" :data-target="'modal-'+site.ID">View Results</button>
                <div class="modal fixed-footer" :id="'modal-'+site.ID" v-if="modalData[site.ID]">
                    <div class="modal-content">
                        <h3 class="modal-header"> {{ site.post_title }} </h3>
                        <ul>
                            <li v-for="response in modalData[site.ID].response_data">
                                <div class="results">
                                    <div v-for="test in JSON.parse(response).results">
                                        <h6>Passes:</h6>
                                        <ul>
                                            <li v-for="pass in test.pageResults.passes"> {{ pass }}</li> 
                                        </ul>

                                        <h6>Fails:</h6>
                                        <ul>
                                            <li v-for="fail in test.pageResults.fails"> {{ fail }}</li> 
                                        </ul>   
                                    </div>
                                </div>
                                
                            </li>
                        </ul>
                    </div>
                    <div class="modal-footer">
                        <a href="#!" class="modal-action modal-close waves-effect btn-flat ">Close</a>
                    </div>
                </div>
             </td>

    </tr>
        </tbody>
    </table>
    <div id="create-test" class="modal modal-fixed-footer">
            <div class="modal-content">
                <h3>Create SEO Test</h3>
                <form class="col s12">
                    <div class="row">
                        <input placeholder="Site Name" id="site_name" v-model="siteName" type="text" />
                        <label for="site_name">Site Name</label>
                    </div>
                    <div class="row group" v-for="(group, index) in formFields">
                        <input placeholder="Product Pages" v-model="formFields[index].groupName" type="text" />
                        <label for="site_name">Group Name</label>
                        <br>
                        <br>
                        <h6>URLs to Test:</h6>
                        <input class="url" v-for="(url, key) in formFields[index].urls" placeholder="http:// or https://" type="text" v-model="formFields[index].urls[key].url" />
                        <br>
                        <br>
                        <button type="button" @click="addUrl(index)"class="btn-floating btn-large waves-effect waves-light"><i class="material-icons">add</i></button>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="col s12">
                    <div class="row">
                    <button class="btn" type="button" @click="addField">
                        New URL Group
                    </button>
                        <a href="#!" @click="submitTest" class="modal-action modal-close waves-effect waves-green btn-flat">Create Test</a>
                    </div>
                </div>
            </div>
    </div>
</div>
EOT;
    wp_enqueue_script('vue');
    wp_enqueue_script('seo-tools-api');
    return $html;
    else:

        $html = <<<EOT
        <h3>You are not logged in</h3>
        <a href=/seo-tools/sign-in" class="button btn text-white">Sign In</a> <a href="/seo-tools/sign-up" class="button btn">Sign up</a>
EOT;
        return $html;
    endif;

}
add_shortcode( 'seo-tools', 'seo_tools_shortcode');

//* Create SEO User Role
add_role('seo_tools_user', 'SEO Tools User', array('read'));

//* Hide Admin Bar for SEO Tools Users

add_action('set_current_user', 'seo_tools_hide_admin_bar');
function seo_tools_hide_admin_bar() {
  if (!current_user_can('edit_posts')) {
    show_admin_bar(false);
  }
}

add_filter( 'acf/settings/remove_wp_meta_box', '__return_false' );
